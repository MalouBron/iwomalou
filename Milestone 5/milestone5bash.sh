#!/bin/bash
# Cat —> laat alle inhoud zien van file
# Grep -Eo ‘\w+’ —> elk woord op een aparte regel
# Grep -wi de—> i is case insensitive en dan grept die op elke "de" of "De"
# Wc -w —> telt alle woorden die over zijn

cat  RUG_wiki_page.txt | grep -Eo '\w+' | grep -wi 'de' | wc -w